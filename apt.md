### Preparación del sistema operativo

#### Verificar el archivo `sources.list`

Verificar que el contenido del archivo `/etc/apt/sources.list` tenga los repositorios y la versión correcta

Se puede comparar el contenido actual del archivo con [sources.list] y reemplazarlo en caso de ser necesario

[sources.list](https://gist.github.com/tonejito/81b995ddbed62bc3d0f1ab6a36709729#file-sources-list-8_jessie "sources.list for Debian 8 Jessie")

#### Verificar que el sistema esté actualizado

Ejecutar `aptitude update` para actualizar la información de los repositorios de paquetes y verificar que no existan actualizaciones pendientes

```sh
root@debian:~# aptitude update
Get: 1 http://security.debian.org jessie/updates InRelease [63.1 kB]
Get: 2 http://security.debian.org jessie/updates/main Sources [199 kB]
Ign http://httpredir.debian.org jessie InRelease
Get: 3 http://httpredir.debian.org jessie-updates InRelease [145 kB]
Hit http://security.debian.org jessie/updates/contrib Sources
Get: 4 http://httpredir.debian.org jessie-backports InRelease [166 kB]

	...

Get: 27 http://httpredir.debian.org jessie/non-free amd64 Packages [83.6 kB]
Fetched 10.24 MB in 7s (1.44 MB/s)
Current status: 21 updates [+21], 43270 new [+43270].

```

#### Descargar e instalar actualizaciones

En caso de existir actualizaciones pendientes, descargarlas e instalarlas

```sh
root@debian:~# aptitude safe-upgrade
The following packages will be upgraded:
  base-files debconf debconf-i18n debian-archive-keyring gnupg gpgv host libc-bin libc6
  libgcrypt20 libpam-modules libpam-modules-bin libpam0g linux-compiler-gcc-4.8-x86
  linux-headers-3.16.0-4-amd64 linux-headers-3.16.0-4-common linux-image-3.16.0-4-amd64 locales
  multiarch-support os-prober tzdata
The following packages are RECOMMENDED but will NOT be installed:
  firmware-linux-free gnupg-curl irqbalance
21 packages upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Need to get 52.5 MB of archives. After unpacking 125 kB will be freed.
Do you want to continue? [Y/n/?] y
Get: 1 http://security.debian.org/ jessie/updates/main linux-image-3.16.0-4-amd64 amd64 3.16.43-2+deb8u5 [34.0 MB]
Get: 2 http://httpredir.debian.org/debian/ jessie/main base-files amd64 8+deb8u9 [78.2 kB]
Get: 3 http://httpredir.debian.org/debian/ jessie/main libc-bin amd64 2.19-18+deb8u10 [1,281 kB]

	...

Get: 21 http://security.debian.org/ jessie/updates/main linux-headers-3.16.0-4-common amd64 3.16.43-2+deb8u5 [4,557 kB]
Fetched 51.2 MB in 35.5s (1.44 MB/s)

	...

Reading changelogs... Done
Preconfiguring packages ...
(Reading database ... 39624 files and directories currently installed.)
Preparing to unpack .../base-files_8+deb8u9_amd64.deb ...
Unpacking base-files (8+deb8u9) over (8+deb8u8) ...
Processing triggers for install-info (5.2.0.dfsg.1-6) ...
Processing triggers for man-db (2.7.0.2-5) ...
Setting up base-files (8+deb8u9) ...
Installing new version of config file /etc/debian_version ...

	...

(Reading database ... 39625 files and directories currently installed.)
Preparing to unpack .../os-prober_1.65+deb8u1_amd64.deb ...
Unpacking os-prober (1.65+deb8u1) over (1.65) ...
Setting up os-prober (1.65+deb8u1) ...

Current status: 0 updates [-21].
```

#### Reiniciar el sistema después de instalar actualizaciones

Cuando el sistema se encuentre actualizado, reiniciarlo

```sh
root@debian:~# reboot & exit ;
```

#### Notas

>>>
Estos pasos están cubiertos en el script `apt-first-aid` ubicado en la siguiente dirección:

+ https://gist.github.com/tonejito/540ace6adeb3c01880375689801200f9
>>>
